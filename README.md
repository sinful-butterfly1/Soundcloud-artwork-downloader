# Soundcloud-artwork-downloader
A userscript for tampermonkey that allows you to download artworks from soundcloud. 
It's still a wip, it's very limited but it does what the title says. 


## Usage
- Install it from there: [script](https://openuserjs.org/install/mirro-chan/Artwork_downloader_for_Soundcloud.user.js) 
- Be logged out of soundcloud
- Go on a song page (eg. https://soundcloud.com/by1997/1997-x-luxxgrve-its-gonna-kill-me)


Then the script will replace the buttons at the bottom with a link that takes you to the artwork like so:

![pic](preview.png)

// ==UserScript==
// @name         Artwork downloader for Soundcloud
// @namespace    https://openuserjs.org/users/mirro-chan
// @version      0.1
// @description  A simple user script to download artworks from soundcloud.
// @author       mirro
// @copyright 2019, mirro-chan (https://openuserjs.org/users/mirro-chan)
// @license MIT
// @match        https://soundcloud.com/*
// @grant        none
// ==/UserScript==

// ==OpenUserJS==
// @author mirro-chan
// ==/OpenUserJS==

(function () {
  'use strict';
  let url = "";
  try {
    url = document.getElementsByClassName("sc-artwork")[0].firstElementChild.style.backgroundImage;
    let regex = new RegExp('/([^"]*)');
    let matches = regex.exec(url);
    document.getElementsByClassName("sc-button-group")[0].innerHTML = `<a href="https:/${matches[1]}" download>Download artwork</a>`;
  }
  catch (e) {
    alert("Cannot find Artwork. You need to be logged out and on a song page.");
  }
})();
